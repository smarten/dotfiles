#!/bin/bash

extensions=(
  # Theming
  vscode-icons-team.vscode-icons

  # SSH
  ms-vscode-remote.remote-ssh
  ms-vscode-remote.remote-ssh-edit
  ms-vscode-remote.remote-wsl

  # CSharp
  ms-dotnettools.csharp
  k--kato.docomment
  fudge.auto-using
  fernandoescolar.vscode-solution-explorer
  jmrog.vscode-nuget-package-manager

  # Others
  editorconfig.editorconfig
  wayou.vscode-todo-highlight
  eamodio.gitlens
  yzhang.markdown-all-in-one
)

for ext in "${extensions[@]}"; do
  code --install-extension $ext
done
